import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './components/Header'
import { uuid }from 'uuidv4'

export default function App() {
    const [items, setItems] = useState([
    {id: uuid(), text:'eggs'},
    {id: uuid(), text:'milk'},
    {id: uuid(), text:'salt'},
    {id: uuid(), text:'wtf'},
    ])
  return (
    <View style={styles.container}>
      <Header />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
