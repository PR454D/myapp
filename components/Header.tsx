import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Header({title}) {
  return (
    <View style={styles.header}>
      <Text style={styles.textbox}>{title}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

Header.defaultProps = {
    title: 'mySuperCoolIdea',
}

const styles = StyleSheet.create({
  header: {
    height: 60,
    padding: 20,
    backgroundColor: 'darkslateblue',
  },
  textbox: {
    color: "#bedd0f",
    fontSize: 25,
    textAlign: 'center',
  }
});
